# MERN-Stack Back-end Example

how to install and start the backend
```
npm install express body-parser cors mongoose
npm install -g nodemon
```

to start the backend
`nodemon server`

how to install mongodb
```
brew install mongodb
mkdir -p /data/db
```

creating a new mongo db for this app
```
mongo
use todos
```

start the mongodb
`mongod`